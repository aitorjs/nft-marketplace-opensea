
const web3 = require("web3");
const { OpenSeaPort, Network } = require("opensea-js");

const API_KEY = process.env.NODE_KEY
const MUMBAI = `https://rpc-mumbai.maticvigil.com/v1/${API_KEY}`

// This example provider won't let you make transactions, only read-only calls:
const provider = new Web3.providers.HttpProvider(MUMBAI)
const openseaApiKey = "";
const seaport = new OpenSeaPort(provider, {
    networkName: Network.Testnet,
    apiKey: openseaApiKey
})


const asset = await seaport.api.getAsset({
    tokenAddress: "0x6F8CB5b502E0142C7D077fb72377b4df9FE902D6", // string
    tokenId: "1", // string | number | null
})


curl--request GET--url 'https://testnets-api.opensea.io/assets?owner=0x86Bf8d68Ed0fF2596CDdEFAbB9Adb1C7E506557E&order_direction=desc&offset=0&limit=20'
